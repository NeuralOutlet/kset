#ifndef SAFE_OBJECT_H
#define SAFE_OBJECT_H

#include <typeinfo>
#include <cstring>
#include <cstdio>
#include <new>


union safe_obj;

// forward friends
template<class T>
T from(safe_obj &_obj);

struct base_obj
{
	virtual ~base_obj() {}
	virtual size_t size() const = 0;
};

template<class T>
struct type_obj : public base_obj
{
	type_obj() {}
	type_obj(T _obj) { data = _obj; }
	virtual ~type_obj() {}
	virtual size_t size() const { return sizeof(type_obj<T>); }

	T data;
};

// some typedefs
typedef decltype(nullptr) nullptr_t;
#define type_t type_obj<T>

/*!
 *
 *
 */
union safe_obj
{
	template<class T>
	friend T from(safe_obj &_obj);

public:
	safe_obj();
	~safe_obj()
	{
		if (p_obj)
			delete p_obj;
	}

	safe_obj(const safe_obj &_sobj)
	{
		p_raw = new unsigned char[_sobj.size()];
		memcpy(p_raw, _sobj.p_raw, _sobj.size());
	}

	template<class T>
	safe_obj(T _obj);

	size_t size() const 
	{
		if (p_obj)
			return p_obj->size();
		return 0;
	}

	safe_obj& operator=(const safe_obj& _sobj);

private:
	base_obj *p_obj;
	void *p_raw;
};


/// --------------------- definitions --------------------- ///

template<class T>
T from(safe_obj &_obj) try
{
	type_t &tmp = dynamic_cast<type_t&>(*_obj.p_obj);
	return tmp.data;
}
catch (...)
{
	return T();
}

safe_obj::safe_obj()
{
	p_raw = nullptr;
}

template<class T>
safe_obj::safe_obj(T _obj)
{
	p_raw = new unsigned char[sizeof(type_t)];
	p_obj = new (p_raw) type_t(_obj);
}


safe_obj& safe_obj::operator=(const safe_obj& _sobj)
{
	if (p_obj)
		delete p_obj;

	p_raw = new unsigned char[_sobj.size()];
	memcpy(p_raw, _sobj.p_raw, _sobj.size());
	return *this;
}

#endif
