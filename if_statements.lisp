
;; FORTRAN77
(defmacro arithmetic-if (value lt eq gt)
	`(if (< ,value 0)
		(progn ,lt)
		(if (> ,value 0)
			(progn ,gt)
			(progn ,eq))))

(arithmetic-if -1
	(format t "Less than~%")
	(format t "Equal to~%")
	(format t "Greater than~%"))



;; IF-ELSE-OTHERWISE
(defmacro dynamic-if (condition tbody fbody &rest dbody)
	`(let ((+other+ ,condition))
		(if (equal t +other+)
			,tbody
			(if +other+
				(progn ,@dbody)
				,fbody))))

(dynamic-if (+ 6 6 6)
	(format t "TRUE~%")
	(format t "FALSE~%")

	(format t "--------~%")
	(format t ">> ~a <<~%" +other+)
	(format t "--------~%"))

