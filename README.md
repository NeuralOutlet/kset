# KSet: Chaotic Set #

## The Concept ##

A container that can *hold any mix of types, without duplicates, and is rigorously unordered.* The attribute of rigorous unorder is not just pseudorandomness, by this I mean the set is not *definately* going to change order, it just might.

## The Language ##

A very barebones language with four basic types {set, symbol, number, bool}, a Pythonic syntax (tab-scoped), amb not much else.

### Sets ###

* constructed manually or via set-builder notation
* Sets are made of an internal type called 'elements'
* * elements can be ordernal to create lists from sets

### Syntax Ideas ###

Variable and function are defined on symbols (which contain {value, function, property} cells like in Lisp 2).

    define <symbol name> := <value>
    define <symbol name> := <set builder notation>
    define <symbol name> <params...>
        <progn>
        <return value>

IF/ELSE clause, has an optional OTHERWISE clause for non-boolean results of the condition. Inside the OTHERWISE scope the result of condition is stored in a variable called 'cond'.

    if <condition>
        <progn>
    else
        <progn>
    [otherwise <progn>]

Iteration methods can be navigated with 'continue' and 'break' keywords. symbols have if they are a sequence in the property cell. 'target' is a symbol that only exists for the scope of LOOP.

    loop using <target> : <sequence>
        <progn>
        
    loop while <condition>
        <progn>
    [otherwise <progn>]

### Example ###

    define foo := {1 2 3 4}
    define printItems bar N
        define count := 0
        loop while count < N
            print bar[count]
    
    define test val
        if val
            {x*2 : {1 2 3 4}}
        else
            {x^2 : {1 2 3 4}}
        otherwise
            if isSet cond
                cond
            else
                {1 2 3 4}
    
    define aye := test(true)
    printItems aye |aye|
    
    define bee := test(foo)
    printItems bee |bee|
    
    define cee := test(false)
    printItems cee |cee|
	
### My Approach to unorder ###

Threaded searches will hopefully achieve something that *feels* unordered without just being rand() % set.size(). 
	