#include <cstdio>
#include <string>
#include "safe_object.h"

int main()
{
	std::string strTest = "Aa";
	std::string strTest2 = "BB";
	printf("%d\n", strTest[0] + strTest[1]);
	printf("%d\n", strTest2[0] + strTest2[1]);

	safe_obj test(666);
	safe_obj test2("test");
	safe_obj test3;
	safe_obj test4(333);
	safe_obj test5 = test4;
	test3 = test;
	test3 = test2;
	test3 = test5;

	printf("%d, %s, %d, %d, %d\n",
	       from<int>(test),
	       from<const char *>(test2),
	       from<int>(test3),
	       from<int>(test4),
	       from<int>(test5));

	printf("%c, %.2f\n",
	       from<char>(test),
	       from<float>(test2));

	return 0;
}

